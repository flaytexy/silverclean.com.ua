<?php
// Text
$_['text_items']     = '%s - %s';
$_['text_empty'] = 'Ваш кошик порожній!';
$_['text_cart'] = 'Перейти до кошику';
$_['text_checkout'] = 'Оформити замовлення';
$_['text_recurring'] = 'Платіжний профіль';
$_['text_continue_shopping'] = 'Продовжити покупки';
$_['text_to_cart'] = 'Перейти до кошика';