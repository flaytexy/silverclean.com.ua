<?php
// *	@source		See SOURCE.txt for source and other copyright.
// *	@license	GNU General Public License version 3; see LICENSE.txt

// Text
$_['text_search']              = 'Поиск';
$_['text_brand']               = 'Производитель';
$_['text_manufacturer']        = 'Производитель:';
$_['text_model']               = 'Код Товара:';
$_['text_reward']              = 'Бонусные баллы:';
$_['text_points']              = 'Цена в бонусных баллах:';
$_['text_stock']               = 'Наличие:';
$_['text_instock']             = 'В наличии';
$_['text_tax']                 = 'Без НДС:';
$_['text_discount']            = ' или больше ';
$_['text_option']              = 'Доступные варианты';
$_['text_minimum']             = 'Минимальное количество заказа этого товара %s';
$_['text_reviews']             = '%s отзывов';
$_['text_write']               = 'Написать отзыв';
$_['text_login']               = 'Пожалуйста <a href="%s">авторизируйтесь</a> или <a href="%s">создайте учетную запись</a> перед тем как написать отзыв';
$_['text_no_reviews']          = 'Нет отзывов об этом товаре.';
$_['text_note']                = '<span style="color: #FF0000;">Примечание:</span> HTML разметка не поддерживается! Используйте обычный текст.';
$_['text_success']             = 'Спасибо за ваш отзыв. Он поступил администратору для проверки на спам и вскоре будет опубликован.';
$_['text_related']             = 'Рекомендуемые товары';
$_['text_tags']                = 'Теги:';
$_['text_error']               = 'Товар не найден!';
$_['text_payment_recurring']   = 'Платежный профиль';
$_['text_trial_description']   = 'Сумма: %s; Периодичность: %d %s; Кол-во платежей: %d, Далее ';;
$_['text_payment_description'] = 'Сумма: %s; Периодичность:  %d %s; Кол-во платежей:  %d ';
$_['text_payment_cancel']      = 'Сумма: %s; Периодичность:  %d %s(s) ; Кол-во платежей: до отмены';
$_['text_day']                 = 'День';
$_['text_week']                = 'Неделя';
$_['text_semi_month']          = 'Полмесяца';
$_['text_month']               = 'Месяц';
$_['text_year']                = 'Год';
$_['text_benefits']     = 'Преимущества:';

$_['text_price_2']     = 'Цена:';
$_['text_buy_goods_cheaper']     = 'Купить товар дешевле';
$_['text_product_code']     = 'Код товара:';
$_['url_buy_goods_cheaper']     = 'kupit-tovar-deshevle';
$_['text_seo_1']     = 'Отправка по Украине в день заказа до 16:00';
$_['text_seo_2']     = '100% гарантия получения и возврата';
$_['text_seo_3']     = 'Соответствие всем нормам и правилам';
$_['text_seo_4']     = 'Конкурентные цены и никакого обмана';
$_['text_buy_one_name']     = 'Введите имя';
$_['text_buy_one_submit']     = 'Купить товар в 1 клик';

$_['text_title_popup_buy_click']     = 'Купить товар дешевле';
$_['text_name']     = 'Имя';
$_['text_phone']     = 'Телефон';
$_['text_count']     = 'Количество';
$_['text_order_popup_buy_click']     = 'Отправить заказ';

$_['text_information']     = 'Информация';
$_['text_information_text']     = 'Актуальную цену на товар уточняйте у менеджера так как товар имеет статус "под заказ" и после прихода цена может изменится';
// Entry
$_['entry_qty']                = 'Кол-во';
$_['entry_name']               = 'Ваше имя';
$_['entry_review']             = 'Ваш отзыв';
$_['entry_rating']             = 'Рейтинг';
$_['entry_good']               = 'Хорошо';
$_['entry_bad']                = 'Плохо';

// Tabs
$_['tab_description']          = 'Описание';
$_['tab_attribute']            = 'Характеристики';
$_['tab_review']               = 'Отзывов (%s)';

// Error
$_['error_name']               = 'Имя должно содержать от 3 до 25 символов!';
$_['error_text']               = 'Текст отзыва должен содержать от 25 до 1000 символов!';
$_['error_rating']             = 'Пожалуйста, выберите оценку!';