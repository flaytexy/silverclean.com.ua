<?php
if(is_dir($_SERVER["DOCUMENT_ROOT"].'image/')){
    define('DOCUMENT_ROOT',$_SERVER["DOCUMENT_ROOT"]);
}else{
    define('DOCUMENT_ROOT',$_SERVER["DOCUMENT_ROOT"].'/');
}

define('HOST',$_SERVER['SERVER_NAME']);

// HTTP
define('HTTP_SERVER', 'https://'.HOST.'/');
// HTTPS
define('HTTPS_SERVER', 'https://'.HOST.'/');

// DIR
define('DIR_APPLICATION', DOCUMENT_ROOT.'catalog/');
define('DIR_SYSTEM', DOCUMENT_ROOT.'system/');
define('DIR_IMAGE', DOCUMENT_ROOT.'image/');
define('DIR_STORAGE', DOCUMENT_ROOT.'storage/');
define('DIR_LANGUAGE', DIR_APPLICATION . 'language/');
define('DIR_TEMPLATE', DIR_APPLICATION . 'view/theme/');
define('DIR_CONFIG', DIR_SYSTEM . 'config/');
define('DIR_CACHE', DIR_STORAGE . 'cache/');
define('DIR_DOWNLOAD', DIR_STORAGE . 'download/');
define('DIR_LOGS', DIR_STORAGE . 'logs/');
define('DIR_MODIFICATION', DIR_STORAGE . 'modification/');
define('DIR_SESSION', DIR_STORAGE . 'session/');
define('DIR_UPLOAD', DIR_STORAGE . 'upload/');

// DB
define('DB_DRIVER', 'mysqli');
define('DB_HOSTNAME', 'localhost');
define('DB_USERNAME', 'u_new_silver8');
define('DB_PASSWORD', 'nJ0XUTBO');
define('DB_DATABASE', 'new_silverclean_com_ua');
define('DB_PORT', '3306');
define('DB_PREFIX', 'oc_');