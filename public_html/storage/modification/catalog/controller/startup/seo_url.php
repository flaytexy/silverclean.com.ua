<?php
// *	@source		See SOURCE.txt for source and other copyright.
// *	@license	GNU General Public License version 3; see LICENSE.txt

class ControllerStartupSeoUrl extends Controller {
	
	//seopro start
		private $seo_pro;
		public function __construct($registry) {
			parent::__construct($registry);	
			$this->seo_pro = new SeoPro($registry);
		}
	//seopro end
	
	public function index() {

		// Add rewrite to url class
		if ($this->config->get('config_seo_url')) {
			$this->url->addRewrite($this);

      // OCFilter start
      if ($this->registry->has('ocfilter')) {
  			$this->url->addRewrite($this->ocfilter);
  		}
      // OCFilter end
      
		}
		
	
		// Decode URL
		if (isset($this->request->get['_route_'])) {
			$parts = explode('/', $this->request->get['_route_']);
			
		//seopro prepare route
		if($this->config->get('config_seo_pro')){		
			$parts = $this->seo_pro->prepareRoute($parts);
		}
		//seopro prepare route end

			// remove any empty arrays from trailing
			if (utf8_strlen(end($parts)) == 0) {
				array_pop($parts);
			}

			foreach ($parts as $part) {
				$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "seo_url WHERE keyword = '" . $this->db->escape($part) . "' AND store_id = '" . (int)$this->config->get('config_store_id') . "'");

				if ($query->num_rows) {
					$url = explode('=', $query->row['query']);

					if ($url[0] == 'product_id') {
						$this->request->get['product_id'] = $url[1];
					}

					if ($url[0] == 'category_id') {
						if (!isset($this->request->get['path'])) {
							$this->request->get['path'] = $url[1];
						} else {
							$this->request->get['path'] .= '_' . $url[1];
						}
					}


//technics start
					if ($url[0] == 'technicscatblog_id') { 
						if (!isset($this->request->get['lbpath'])) {

							$this->request->get['lbpath'] = $url[1];
						} else {
							$this->request->get['lbpath'] = $url[1];
						}
					}

					if ($url[0] == 'news_id') {
						$this->request->get['news_id'] = $url[1];
					}

					if ($url[0] == 'blog_id') {
						$this->request->get['blog_id'] = $url[1];
					}
//technics end
			
					if ($url[0] == 'manufacturer_id') {
						$this->request->get['manufacturer_id'] = $url[1];
					}

					if ($url[0] == 'information_id') {
						$this->request->get['information_id'] = $url[1];
					}

					if ($query->row['query'] && $url[0] != 'information_id' && $url[0] != 'blog_id' && $url[0] != 'news_id' && $url[0] != 'technicscatblog_id' && $url[0] != 'manufacturer_id' && $url[0] != 'category_id' && $url[0] != 'product_id') {
						$this->request->get['route'] = $query->row['query'];
					}
				} else {
					if(!$this->config->get('config_seo_pro')){		
						$this->request->get['route'] = 'error/not_found';
					}

					break;
				}
			}

			if (!isset($this->request->get['route'])) {
				if (isset($this->request->get['product_id'])) {
					$this->request->get['route'] = 'product/product';
				} elseif (isset($this->request->get['path'])) {
					$this->request->get['route'] = 'product/category';
				} elseif (isset($this->request->get['manufacturer_id'])) {
					$this->request->get['route'] = 'product/manufacturer/info';
				} elseif (isset($this->request->get['information_id'])) {
					$this->request->get['route'] = 'information/information';

				} elseif (isset($this->request->get['news_id'])) {//technics
					$this->request->get['route'] = 'extension/module/technics_news/getnews';//technics
				} elseif (isset($this->request->get['blog_id'])) {//technics
					$this->request->get['lbpath'] = '2';

					$this->request->get['route'] = 'extension/module/technics_blog/getblog';//technics
				} elseif (isset($this->request->get['lbpath'])) {//technics

					$this->request->get['route'] = 'extension/module/technicscat_blog/getcat';//technics
			
				}
			}
		}
		
		//seopro validate
		if($this->config->get('config_seo_pro')){		
			$this->seo_pro->validate();
		}
	//seopro validate
		
	}


//technics start
	public function getPathByBlog($blog_id) {
		$blog_id = (int)$blog_id;
		if ($blog_id < 1) return false;

		static $path = null;
		$query = $this->db->query("SELECT category_id FROM " . DB_PREFIX . "technics_blog_to_category WHERE blog_id = '" . (int)$blog_id . "' ORDER BY main_category DESC LIMIT 1");

		$path[$blog_id] = $this->getPathByCategoryLb($query->num_rows ? (int)$query->row['category_id'] : 0);

		return $path[$blog_id];
	}

	private function getPathByCategoryLb($category_id) {
		$category_id = (int)$category_id;
		if ($category_id < 1) return false;

		static $path = null;

			$max_level = 10;

			$sql = "SELECT CONCAT_WS('_'";
			for ($i = $max_level-1; $i >= 0; --$i) {
				$sql .= ",t$i.category_id";
			}
			$sql .= ") AS path FROM " . DB_PREFIX . "technicscat_blog t0";
			for ($i = 1; $i < $max_level; ++$i) {
				$sql .= " LEFT JOIN " . DB_PREFIX . "technicscat_blog t$i ON (t$i.category_id = t" . ($i-1) . ".parent_id)";
			}
			$sql .= " WHERE t0.category_id = '" . (int)$category_id . "'";

			$query = $this->db->query($sql);

			$path[$category_id] = $query->num_rows ? $query->row['path'] : false;

		return $path[$category_id];
	}
//technics end
			
	public function rewrite($link) {
		$url_info = parse_url(str_replace('&amp;', '&', $link));

		if($this->config->get('config_seo_pro')){		
		$url = null;
			} else {
		$url = '';
		}

		$data = array();

		parse_str($url_info['query'], $data);
		
		//seo_pro baseRewrite
		if($this->config->get('config_seo_pro')){		
			list($url, $data, $postfix) =  $this->seo_pro->baseRewrite($data, (int)$this->config->get('config_language_id'));	
		}
		
		

		
		//seo_pro baseRewrite

		foreach ($data as $key => $value) {
			if (isset($data['route'])) {
				if (($data['route'] == 'product/product' && $key == 'product_id') || ($data['route'] == 'extension/module/technics_news/getnews' && $key == 'news_id') || (($data['route'] == 'product/manufacturer/info' || $data['route'] == 'product/product') && $key == 'manufacturer_id') || ($data['route'] == 'information/information' && $key == 'information_id')) {
					$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "seo_url WHERE `query` = '" . $this->db->escape($key . '=' . (int)$value) . "' AND store_id = '" . (int)$this->config->get('config_store_id') . "' AND language_id = '" . (int)$this->config->get('config_language_id') . "'");

					if ($query->num_rows && $query->row['keyword']) {
						$url .= '/' . $query->row['keyword'];

						unset($data[$key]);
					}

//technics start
				} elseif ($data['route'] == 'extension/module/technics_blog/getblog' && $key == 'blog_id') {
					if ($this->config->get('config_seo_pro') === null) {  // Check - This section for Russian build OCStore only

						if ($this->config->get('theme_technics_blog_path') ) { 
							$catlessblog = false;
							$categories = $this->getPathByBlog($value); 
							if (!$categories) $catlessblog = true;
							if ($catlessblog) {
								$categories = 0;
							}
							$categories = explode('_', $categories);
							if ($categories[0]) {
								array_unshift($categories, "0");
							}

							foreach ($categories as $category) {
								$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "seo_url WHERE `query` = 'technicscatblog_id=" . (int)$category . "'");

								if ($query->num_rows && $query->row['keyword']) {
									$url .= '/' . $query->row['keyword'];
								} else {
									$url = '';

									break;
								}
							}

						}
					}
					$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "seo_url WHERE `query` = '" . $this->db->escape($key . '=' . (int)$value) . "' AND store_id = '" . (int)$this->config->get('config_store_id') . "' AND language_id = '" . (int)$this->config->get('config_language_id') . "'");

					if ($query->num_rows && $query->row['keyword']) {
						$url .= '/' . $query->row['keyword'];

						unset($data[$key]);
					}

				} elseif ($key == 'lbpath') {


					$categories = explode('_', $value);
					if ($categories[0]) {
						array_unshift($categories, "0");
					}

					foreach ($categories as $category) {
						$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "seo_url WHERE `query` = 'technicscatblog_id=" . (int)$category . "'");

						if ($query->num_rows && $query->row['keyword']) {
							$url .= '/' . $query->row['keyword'];
						} else {
							$url = '';

							break;
						}
					}

					unset($data[$key]);	
				}elseif (($data['route'] == 'extension/module/technics_news/getnewslist')) {	
					$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "seo_url WHERE `query` = '" . $this->db->escape($value) . "'");

					if ($query->num_rows && $query->row['keyword'] && (strpos($url, $query->row['keyword']) === false)) {
						//&& (strpos($url, $query->row['keyword']) === false) - uses for new version SEO-PRO to avoid doubling
						$url .= '/' . $query->row['keyword'];

						unset($data[$key]);
					}				

//technics end
			
				} elseif ($key == 'path') {
					$categories = explode('_', $value);

					foreach ($categories as $category) {
						$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "seo_url WHERE `query` = 'category_id=" . (int)$category . "' AND store_id = '" . (int)$this->config->get('config_store_id') . "' AND language_id = '" . (int)$this->config->get('config_language_id') . "'");

						if ($query->num_rows && $query->row['keyword']) {
							$url .= '/' . $query->row['keyword'];
						} else {
							$url = '';

							break;
						}
					}

					unset($data[$key]);
				}
			}
		}

		//seo_pro add blank url
		if($this->config->get('config_seo_pro')) {		
			$condition = ($url !== null);
		} else {
			$condition = $url;
		}

		if ($condition) {
			if($this->config->get('config_seo_pro')){		
				if($this->config->get('config_page_postfix') && $postfix) {
					$url .= $this->config->get('config_page_postfix');
				} elseif($this->config->get('config_seopro_addslash')) {
					$url .= '/';
				} 
			}
			
		//seo_pro add blank url
			unset($data['route']);

			$query = '';

			if ($data) {
				foreach ($data as $key => $value) {
					$query .= '&' . rawurlencode((string)$key) . '=' . rawurlencode((is_array($value) ? http_build_query($value) : (string)$value));
				}

				if ($query) {
					$query = '?' . str_replace('&', '&amp;', trim($query, '&'));
				}
			}

			return $url_info['scheme'] . '://' . $url_info['host'] . (isset($url_info['port']) ? ':' . $url_info['port'] : '') . str_replace('/index.php', '', $url_info['path']) . $url . $query;
		} else {
			return $link;
		}
	}
}
