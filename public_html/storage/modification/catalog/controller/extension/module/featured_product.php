<?php
// *	@source		See SOURCE.txt for source and other copyright.
// *	@license	GNU General Public License version 3; see LICENSE.txt

class ControllerExtensionModuleFeaturedProduct extends Controller {
	public function index($setting) {
		
		
		if (!$setting['limit']) {
			$setting['limit'] = 4;
		}
		
		$results = array();
		
		$this->load->model('catalog/cms');
		
			if (isset($this->request->get['manufacturer_id'])) {
					
					$filter_data = array(
						'manufacturer_id'  => $this->request->get['manufacturer_id'],
						'limit' => $setting['limit']
					);
					
					$results = $this->model_catalog_cms->getProductRelatedByManufacturer($filter_data);
				
			} else {
				
					$parts = explode('_', (string)$this->request->get['path']);
					
					if(!empty($parts) && is_array($parts)) {
					
						$filter_data = array(
							'category_id'  => array_pop($parts),
							'limit' => $setting['limit']
						);
						
					$results = $this->model_catalog_cms->getProductRelatedByCategory($filter_data);
								
					}
			}
		
		$this->load->language('extension/module/featured_product');

		$this->load->model('catalog/product');

		$this->load->model('tool/image');

		$data['products'] = array();

		// technics
		$this->load->language('extension/theme/technics');
		$data['lazyload'] = $this->config->get('theme_technics_lazyload');
		$data['category_time'] = $this->config->get('theme_technics_category_time');
		$data['time_text_1'] = $this->language->get('text_time_text_1');
		$data['time_text_2'] = $this->language->get('text_time_text_2');
		// labels
			$this->load->model('extension/module/technics');
			$labelsInfo = array();
			if($this->config->get('theme_technics_label')){
				$labelsInfo = $this->config->get('theme_technics_label');
			}
			$data['language_id'] = $this->config->get('config_language_id');
			$newest = array();
			$sales = false;
			if(isset($labelsInfo['new']['period']) && $labelsInfo['new']['status']){
				$newest = $this->model_catalog_product->getNewestProducts($labelsInfo['new']['period']);			
			}
			if(isset($labelsInfo['sale']['status']) && $labelsInfo['sale']['status']){
				$sales = true;				
			}	
			$data['labelsinfo'] = $labelsInfo;		
		    if (isset($labelsInfo['hit']) && $labelsInfo['hit']['status']) {
		       $hits = $this->model_extension_module_technics->getHitProducts($labelsInfo['hit']['period'],$labelsInfo['hit']['qty']);
		    }			
		// labels	
		// technics end												
            
		
		if (!empty($results)) {
			
			foreach ($results as $product) {

				if ($product) {
					if ($product['image']) {
						$image = $this->model_tool_image->resize($product['image'], $setting['width'], $setting['height']);
					} else {
						$image = $this->model_tool_image->resize('placeholder.png', $setting['width'], $setting['height']);
					}

					if ($this->customer->isLogged() || !$this->config->get('config_customer_price')) {
						$price = $this->currency->format($this->tax->calculate($product['price'], $product['tax_class_id'], $this->config->get('config_tax')), $this->session->data['currency']);
					} else {
						$price = false;
					}

					if ((float)$product['special']) {
						$special = $this->currency->format($this->tax->calculate($product['special'], $product['tax_class_id'], $this->config->get('config_tax')), $this->session->data['currency']);
					} else {
						$special = false;
					}

					if ($this->config->get('config_tax')) {
						$tax = $this->currency->format((float)$product['special'] ? $product['special'] : $product['price'], $this->session->data['currency']);
					} else {
						$tax = false;
					}

					if ($this->config->get('config_review_status')) {
						$rating = $product['rating'];
					} else {
						$rating = false;
					}
					


					// technics
					
					$extraImages = array();
					if ($this->config->get('theme_technics_images_status')) {
						$images = $this->model_catalog_product->getProductImages($product['product_id']);
						foreach($images as $imageX){
							$extraImages[] = $this->model_tool_image->resize($imageX['image'], $setting['width'], $setting['height']);
						}
					}
					
					if (in_array($product['product_id'], $newest)) {
						$isNewest = true;
					} else {
						$isNewest = false;
					}
					
					$discount = '';
					if($sales && $special){
						$special_date_end = false;
						$action = $this->model_catalog_product->getProductActions($product['product_id']);
						if ($action['date_end'] != '0000-00-00') {
							$special_date_end = $action['date_end'];
						}		

						if($labelsInfo['sale']['extra'] == 1){
							$discount = round((($product['price'] - $product['special'])/$product['price'])*100);
							$discount = $discount. ' %';

						}
						if($labelsInfo['sale']['extra'] == 2){
							$discount = $this->currency->format($this->tax->calculate(($product['price'] - $product['special']), $product['tax_class_id'], $this->config->get('config_tax')), $this->session->data['currency']);
						}					
					} else {
						$special_date_end = false;
					}
				
					$catch = false;
					$nocatch = false;
					if (isset($labelsInfo['catch']) && $labelsInfo['catch']['status'] && $product['quantity'] <= $labelsInfo['catch']['qty']) {
						if($product['quantity'] > 0){
							$catch = $labelsInfo['catch']['name'][$this->config->get('config_language_id')];
						}else{
							$catch = $labelsInfo['catch']['name1'][$this->config->get('config_language_id')];
							$nocatch = true;
						}
					}

					$popular = false;
					if (isset($labelsInfo['popular']) && $labelsInfo['popular']['status'] && $product['viewed'] >= $labelsInfo['popular']['views']) {
						$popular = $labelsInfo['popular']['name'][$this->config->get('config_language_id')];
					}

					$hit = false;
					if (isset($labelsInfo['hit']) && $labelsInfo['hit']['status']) {
						if (isset($hits[$product['product_id']])) {
							$hit = $labelsInfo['hit']['name'][$this->config->get('config_language_id')];
						}
					}			
					
					if ($this->config->get('theme_technics_manufacturer') == 1) {
						$manufacturer = $product['model'];
					} elseif ($this->config->get('theme_technics_manufacturer') == 2) {
						$manufacturer = $product['manufacturer'];
					} else {
						$manufacturer = false;
					}
					
					if ($product['quantity'] <= 0) {
						$stock = $product['stock_status'];
					} elseif ($this->config->get('config_stock_display')) {
						$stock = $product['quantity'];
					} else {
						$stock = $this->language->get('text_instock');
					}	
					
					if ($product['quantity'] <= 0 && !$this->config->get('config_stock_checkout')) {
						$buy_btn = $product['stock_status'];
					} else {
						$buy_btn = '';
					}
				
					// technics end

            
					$data['products'][] = array(
						'product_id'  => $product['product_id'],
						'thumb'       => $image,
						'name'        => $product['name'],
						'description' => utf8_substr(strip_tags(html_entity_decode($product['description'], ENT_QUOTES, 'UTF-8')), 0, $this->config->get('theme_' . $this->config->get('config_theme') . '_product_description_length')) . '..',
						'price'       => $price,

					// technics
					'manufacturer'     => $manufacturer,
					'quantity'         => $product['quantity'],
					'minimum'          => $product['minimum'] > 0 ? $product['minimum'] : 1,
					'stock'            => $stock,
					'images'           => $extraImages,	
					'isnewest'         => $isNewest,
					'sales'       	   => $sales,
					'discount'         => $discount,
					'catch'       	   => $catch,
					'nocatch'          => $nocatch,
					'popular'	       => $popular,
					'hit'	 	       => $hit,
					'buy_btn'	       => $buy_btn,
					'reward'           => $product['reward'],
					'special_date_end' => $special_date_end,
					// technics
            
						'special'     => $special,
						'tax'         => $tax,
						'rating'      => $rating,
						'href'        => $this->url->link('product/product', 'product_id=' . $product['product_id'])
					);
				}
			}
		}
		
		
					// technics
			if(isset($setting['layout']) && strpos($setting['layout'],'column_') !== false){
				return $this->load->view('extension/module/featured_product_column', $data);
			}else{
				return $this->load->view('extension/module/featured_product', $data);
			}
					// technics end
            

	}
	
}