<?php
if(is_dir($_SERVER["DOCUMENT_ROOT"].'image/')){
    define('DOCUMENT_ROOT',$_SERVER["DOCUMENT_ROOT"]);
}else{
    define('DOCUMENT_ROOT',$_SERVER["DOCUMENT_ROOT"].'/');
}

define('HOST',$_SERVER['SERVER_NAME']);

// HTTP
define('HTTP_SERVER', 'https://'.HOST.'/admin/');
define('HTTP_CATALOG', 'https://'.HOST.'/');

// HTTPS
define('HTTPS_SERVER', 'https://'.HOST.'/admin/');
define('HTTPS_CATALOG', 'https://'.HOST.'/');


// DIR
define('DIR_APPLICATION', DOCUMENT_ROOT.'admin/');
define('DIR_SYSTEM', DOCUMENT_ROOT.'system/');
define('DIR_IMAGE', DOCUMENT_ROOT.'image/');
define('DIR_STORAGE', DOCUMENT_ROOT.'storage/');
define('DIR_CATALOG', DOCUMENT_ROOT.'catalog/');
define('DIR_LANGUAGE', DIR_APPLICATION . 'language/');
define('DIR_TEMPLATE', DIR_APPLICATION . 'view/template/');
define('DIR_CONFIG', DIR_SYSTEM . 'config/');
define('DIR_CACHE', DIR_STORAGE . 'cache/');
define('DIR_DOWNLOAD', DIR_STORAGE . 'download/');
define('DIR_LOGS', DIR_STORAGE . 'logs/');
define('DIR_MODIFICATION', DIR_STORAGE . 'modification/');
define('DIR_SESSION', DIR_STORAGE . 'session/');
define('DIR_UPLOAD', DIR_STORAGE . 'upload/');

// DB
define('DB_DRIVER', 'mysqli');
define('DB_HOSTNAME', 'localhost');
define('DB_USERNAME', 'root');
define('DB_PASSWORD', '');
define('DB_DATABASE', 'new_silverclean_com_ua');
define('DB_PORT', '3306');
define('DB_PREFIX', 'oc_');

// OpenCart API
define('OPENCART_SERVER', 'https://www.opencart.com/');
define('OPENCARTFORUM_SERVER', 'https://opencartforum.com/');
