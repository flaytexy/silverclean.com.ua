<?php

/**
 * @category   OpenCart
 * @package    Branched Sitemap
 * @copyright  © Serge Tkach, 2018 - 2021, http://sergetkach.com/
 */

class ControllerExtensionFeedBranchedSitemap extends Controller {
	private $error = array();

  public function install() {
		// Включаем ЧПУ для карты
		$config_seo_url = $this->config->get('config_seo_url');

		if (!$config_seo_url) {
			$this->db->query("UPDATE `" . DB_PREFIX . "setting` SET `value` = '1' WHERE `key` = 'config_seo_url'");
		}
		
		// Default store
		$store_id = 0; // it is default
		
		//$data['config_language'] = ;
		
		// Назначаем ЧПУ для карты сайта - OC 3 is DIFFERENT
		$sql = "SELECT `keyword` FROM `" . DB_PREFIX . "seo_url` WHERE `query` = 'extension/feed/branched_sitemap'";
		
		$query = $this->db->query($sql);
		
		if ($query->num_rows < 1) {
			$this->db->query("INSERT INTO `" . DB_PREFIX . "seo_url` SET `store_id` = '" . (int)$store_id . "', `language_id`='" . (int)$this->config->get('config_language_id') . "', `query` = 'extension/feed/branched_sitemap', `keyword` = 'branched-sitemap'");
		}
		
		// Назначаем ЧПУ для карты изображений
		$sql = "SELECT `keyword` FROM `" . DB_PREFIX . "seo_url` WHERE `query` = 'extension/feed/branched_sitemap/image'";
		
		$query = $this->db->query($sql);
		
		if ($query->num_rows < 1) {
			$this->db->query("INSERT INTO `" . DB_PREFIX . "seo_url` SET `store_id` = '" . (int)$store_id . "', `language_id`='" . (int)$this->config->get('config_language_id') . "', `query` = 'extension/feed/branched_sitemap/image', `keyword` = 'branched-sitemap-image'");
		}
		
		$this->cache->delete('seo_pro'); // Чтобы ссылка сразу же и работала

		// Включаем SEO PRO, если такой присутствует - IS DIFFERENT FROM 2.x!!
		$sql = "SELECT `value` FROM `" . DB_PREFIX . "setting` WHERE `key` = 'config_seo_pro'";
		
		$query = $this->db->query($sql);

		if ($query->num_rows > 0 && (1 != $query->row['value'])) {
			$this->db->query("UPDATE `" . DB_PREFIX . "setting` SET `value` = '1' WHERE `key` = 'config_seo_pro'");
		}
		
    // Включаем индексы для таблиц
		if (!$this->hasIndex('category_description', 'language_id')) {
			$this->db->query("ALTER TABLE " . DB_PREFIX . "category_description ADD INDEX language_id ( language_id );");
		}

		if (!$this->hasIndex('product_description', 'language_id')) {
			$this->db->query("ALTER TABLE " . DB_PREFIX . "product_description ADD INDEX language_id ( language_id );");
		}

		if (!$this->hasIndex('product_image', 'sort_order')) {
			$this->db->query("ALTER TABLE " . DB_PREFIX . "product_image ADD INDEX sort_order ( sort_order );");
		}

		if (!$this->hasIndex('product_option', 'product_id')) {
			$this->db->query("ALTER TABLE " . DB_PREFIX . "product_option ADD INDEX product_id (product_id);");
		}

		if (!$this->hasIndex('product_option', 'option_id')) {
			$this->db->query("ALTER TABLE " . DB_PREFIX . "product_option ADD INDEX option_id (option_id);");
		}

		if (!$this->hasIndex('product_option_value', 'product_option_id')) {
			$this->db->query("ALTER TABLE " . DB_PREFIX . "product_option_value ADD INDEX product_option_id (product_option_id);");
		}

		if (!$this->hasIndex('product_option_value', 'product_id')) {
			$this->db->query("ALTER TABLE " . DB_PREFIX . "product_option_value ADD INDEX product_id (product_id);");
		}

		if (!$this->hasIndex('product_option_value', 'option_id')) {
			$this->db->query("ALTER TABLE " . DB_PREFIX . "product_option_value ADD INDEX option_id (option_id);");
		}

		if (!$this->hasIndex('product_option_value', 'option_value_id')) {
			$this->db->query("ALTER TABLE " . DB_PREFIX . "product_option_value ADD INDEX option_value_id (option_value_id);");
		}

		if (!$this->hasIndex('product_option_value', 'subtract')) {
			$this->db->query("ALTER TABLE " . DB_PREFIX . "product_option_value ADD INDEX subtract (subtract);");
		}

		if (!$this->hasIndex('product_option_value', 'quantity')) {
			$this->db->query("ALTER TABLE " . DB_PREFIX . "product_option_value ADD INDEX quantity (quantity);");
		}

		if (!$this->hasIndex('product_reward', 'product_id')) {
			$this->db->query("ALTER TABLE " . DB_PREFIX . "product_reward ADD INDEX product_id ( product_id );");
		}

		if (!$this->hasIndex('product_reward', 'customer_group_id')) {
			$this->db->query("ALTER TABLE " . DB_PREFIX . "product_reward ADD INDEX customer_group_id ( customer_group_id );");
		}

		if (!$this->hasIndex('product_to_store', 'store_id')) {
			$this->db->query("ALTER TABLE " . DB_PREFIX . "product_to_store ADD INDEX store_id ( store_id );");
		}

		if (!$this->hasIndex('setting', 'store_id')) {
			$this->db->query("ALTER TABLE " . DB_PREFIX . "setting ADD INDEX store_id ( store_id );");
		}

		if (!$this->hasIndex('setting', 'key')) {
			$this->db->query("ALTER TABLE `" . DB_PREFIX . "setting` ADD INDEX `key` ( `key` );"); // key - reserved word...
		}

		if (!$this->hasIndex('setting', 'serialized')) {
			$this->db->query("ALTER TABLE " . DB_PREFIX . "setting ADD INDEX serialized ( serialized );");
		}

  }

	public function hasIndex($table, $index) {
		$has_index = false;

		$res = $this->db->query("SHOW INDEX FROM " . DB_PREFIX . $table);

		if ($res->num_rows > 0) {
			foreach ($res->rows as $item) {
				if ($index == $item['Column_name']) $has_index = true;
			}
		}

		return $has_index;
	}

	public function index() {
		$this->load->language('extension/feed/branched_sitemap');

		$this->document->setTitle($this->language->get('heading_title'));

		$this->load->model('setting/setting');

    $data['text_success'] = '';

		if ($this->request->server['REQUEST_METHOD'] == 'POST' && $this->validate()) {
			$this->model_setting_setting->editSetting('feed_branched_sitemap', $this->request->post);

			$this->session->data['success'] = $this->language->get('text_success');

			//$this->response->redirect($this->url->link('extension/extension', 'user_token=' . $this->session->data['user_token'] . '&type=feed', true));
      $data['text_success'] = $this->language->get('text_success'); // if no success redirect
		}

    $data['tab_general'] = $this->language->get('tab_general');

		if (isset($this->error['warning'])) {
			$data['error_warning'] = $this->error['warning'];
		} else {
			$data['error_warning'] = '';
		}

		$data['breadcrumbs'] = array();

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_home'),
			'href' => $this->url->link('common/dashboard', 'user_token=' . $this->session->data['user_token'], true)
		);

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_extension'),
			'href' => $this->url->link('marketplace/extension', 'user_token=' . $this->session->data['user_token'] . '&type=feed', true)
		);

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('heading_title'),
			'href' => $this->url->link('extension/feed/branched_sitemap', 'user_token=' . $this->session->data['user_token'], true)
		);

		$data['action'] = $this->url->link('extension/feed/branched_sitemap', 'user_token=' . $this->session->data['user_token'], true);
		$data['cancel'] = $this->url->link('marketplace/extension', 'user_token=' . $this->session->data['user_token'] . '&type=feed', true);

    $data['feed_branched_sitemap_licence'] = '';
    if (isset($this->request->post['feed_branched_sitemap_licence'])) {
      $data['feed_branched_sitemap_licence'] = $this->request->post['feed_branched_sitemap_licence'];
    } else {
      $data['feed_branched_sitemap_licence'] = $this->config->get('feed_branched_sitemap_licence');
    }

    if (version_compare(PHP_VERSION, '7.2') >= 0) {
      $php_v = '72_73';
    } elseif (version_compare(PHP_VERSION, '7.1') >= 0) {
			$php_v = '71';
    } elseif (version_compare(PHP_VERSION, '5.6.0') >= 0) {
      $php_v = '56_70';
    } elseif (version_compare(PHP_VERSION, '5.4.0') >= 0) {
      $php_v = '54_56';
    } else {
      echo "Sorry! Version for PHP 5.3 Not Supported!<br>Please contact to author!";
      exit;
    }

    $file = DIR_SYSTEM . 'library/branched_sitemap/branched_sitemap_' . $php_v . '.php';

    if (is_file($file)){
      require_once $file;
    } else {
      echo "No file '$file'<br>";
      exit;
    }

    $sitemap = new Sitemap();

    $data['valid_licence'] = $sitemap->isValidLicence($data['feed_branched_sitemap_licence']);

		if (isset($this->request->post['feed_branched_sitemap_status'])) {
			$data['feed_branched_sitemap_status'] = $this->request->post['feed_branched_sitemap_status'];
		} else {
			$data['feed_branched_sitemap_status'] = $this->config->get('feed_branched_sitemap_status');
		}
		
		// Feed Links
		$sql = "SELECT `keyword` FROM `" . DB_PREFIX . "seo_url` WHERE `query` = 'extension/feed/branched_sitemap'";
		
		$query = $this->db->query($sql);
		
		if ($query->num_rows > 0) {
			$data['data_feed'] = HTTPS_CATALOG . $query->row['keyword'];
		} else {
			$data['data_feed'] = HTTPS_CATALOG . 'index.php?route=extension/feed/branched_sitemap';
		}
		
		$sql = "SELECT `keyword` FROM `" . DB_PREFIX . "seo_url` WHERE `query` = 'extension/feed/branched_sitemap/image'";
		
		$query = $this->db->query($sql);
		
		if ($query->num_rows > 0) {
			$data['data_feed_image'] = HTTPS_CATALOG . $query->row['keyword'];
		} else {
			$data['data_feed_image'] = HTTPS_CATALOG . 'index.php?route=extension/feed/branched_sitemap/image';
		}

    //$data['systems'] = array('OpenCart', 'ocStore', 'OpenCart.PRO');
    $data['systems'] = array('OpenCart', 'ocStore');

    $data['feed_branched_sitemap_system'] = '';
    if (isset($this->request->post['feed_branched_sitemap_system'])) {
      $data['feed_branched_sitemap_system'] = $this->request->post['feed_branched_sitemap_system'];
    } else {
      $data['feed_branched_sitemap_system'] = $this->config->get('feed_branched_sitemap_system');
    }

    $data['feed_branched_sitemap_require_image_caption'] = '';
    if (isset($this->request->post['feed_branched_sitemap_require_image_caption'])) {
      $data['feed_branched_sitemap_require_image_caption'] = $this->request->post['feed_branched_sitemap_require_image_caption'];
    } else {
      $data['feed_branched_sitemap_require_image_caption'] = $this->config->get('feed_branched_sitemap_require_image_caption');
    }

    $data['feed_branched_sitemap_multishop'] = '';
    if (isset($this->request->post['feed_branched_sitemap_multishop'])) {
      $data['feed_branched_sitemap_multishop'] = $this->request->post['feed_branched_sitemap_multishop'];
    } else {
      $data['feed_branched_sitemap_multishop'] = $this->config->get('feed_branched_sitemap_multishop');
    }

    $data['feed_branched_sitemap_off_description'] = '';
    if (isset($this->request->post['feed_branched_sitemap_off_description'])) {
      $data['feed_branched_sitemap_off_description'] = $this->request->post['feed_branched_sitemap_off_description'];
    } else {
      $data['feed_branched_sitemap_off_description'] = $this->config->get('feed_branched_sitemap_off_description');
    }

		$data['cachetime_values'] = array(
			array('value' => 0, 'text' => $this->language->get('cachetime_values_0')),
			array('value' => 3600, 'text' => $this->language->get('cachetime_values_1hour')),
			array('value' => 21600, 'text' => $this->language->get('cachetime_values_6hours')),
			array('value' => 43200, 'text' => $this->language->get('cachetime_values_12hours')),
			array('value' => 86400, 'text' => $this->language->get('cachetime_values_24hours')),
			array('value' => 604800, 'text' => $this->language->get('cachetime_values_1week')),
		);

		// there is 0 value - so condition elseif($this->config->get('branched_sitemap_cachetime')) { is not good
		
    if (isset($this->request->post['feed_branched_sitemap_cachetime'])) {
      $data['feed_branched_sitemap_cachetime'] = $this->request->post['feed_branched_sitemap_cachetime'];
    } elseif($this->config->get('feed_branched_sitemap_cachetime') || $this->config->get('feed_branched_sitemap_cachetime') === '0') {
      $data['feed_branched_sitemap_cachetime'] = $this->config->get('feed_branched_sitemap_cachetime');
    } else {
      $data['feed_branched_sitemap_cachetime'] = 86400;
    }

    $data['limits'] = array(100, 200, 500, 1000, 3000, 5000);

    if (isset($this->request->post['feed_branched_sitemap_limit'])) {
      $data['feed_branched_sitemap_limit'] = $this->request->post['feed_branched_sitemap_limit'];
    } elseif($this->config->get('feed_branched_sitemap_limit')) {
      $data['feed_branched_sitemap_limit'] = $this->config->get('feed_branched_sitemap_limit');
    } else {
      $data['feed_branched_sitemap_limit'] = 200;
    }
		
    $data['limits_image'] = array(50, 100, 200, 300);

    if (isset($this->request->post['feed_branched_sitemap_limit_image'])) {
      $data['feed_branched_sitemap_limit_image'] = $this->request->post['feed_branched_sitemap_limit_image'];
    } elseif($this->config->get('feed_branched_sitemap_limit')) {
      $data['feed_branched_sitemap_limit_image'] = $this->config->get('feed_branched_sitemap_limit_image');
    } else {
      $data['feed_branched_sitemap_limit_image'] = 100;
    }
		
		$data['feed_branched_sitemap_off_check_image_file'] = '';
    if (isset($this->request->post['feed_branched_sitemap_off_check_image_file'])) {
      $data['feed_branched_sitemap_off_check_image_file'] = $this->request->post['feed_branched_sitemap_off_check_image_file'];
    } else {
      $data['feed_branched_sitemap_off_check_image_file'] = $this->config->get('feed_branched_sitemap_off_check_image_file');
    }

		$data['header'] = $this->load->controller('common/header');
		$data['column_left'] = $this->load->controller('common/column_left');
		$data['footer'] = $this->load->controller('common/footer');

		$this->response->setOutput($this->load->view('extension/feed/branched_sitemap', $data));
	}

	protected function validate() {
		if (!$this->user->hasPermission('modify', 'extension/feed/branched_sitemap')) {
			$this->error['warning'] = $this->language->get('error_permission');
		}

		return !$this->error;
	}
}
