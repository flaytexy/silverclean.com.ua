<?php

/**
 * @category   OpenCart
 * @package    Branched Sitemap
 * @copyright  © Serge Tkach, 2018 - 2021, http://sergetkach.com/
 */

// Heading
$_['heading_title'] = 'Branched Sitemap';

// Text
$_['text_author']         = 'Автор';
$_['text_author_support'] = 'Підтримка';
$_['text_module_version'] = 'Версія модуля';
$_['text_system_version'] = 'Призначено для системи версії';

$_['text_feed']      = 'Канали просування';
$_['text_success']   = 'Налаштування модуля оновлені!';
$_['text_edit']      = 'Редагувати Branched Sitemap';
$_['text_extension'] = 'Канали просування';
$_['text_yes']       = 'Так';
$_['text_no']        = 'Ні';

// Entry
$_['entry_licence']          = 'Код ліцензії:';
$_['entry_status']           = 'Статус:';
$_['entry_system']           = 'Використовувана система:';
$_['entry_cachetime']        = 'Кешувати карту сайту на:';
$_['cachetime_values_0']     = 'Не кешувати';
$_['cachetime_values_1hour'] = '1 годину';
$_['cachetime_values_6hours'] = '6 годин';
$_['cachetime_values_12hours'] = '12 годин';
$_['cachetime_values_24hours'] = '1 день';
$_['cachetime_values_1week'] = 'Тиждень';
$_['entry_limsit']            = 'Ліміт товарів в одному файлі:';
$_['help_limit']             = 'Чим слабкіше сервер, тим менше повинно бути значення';
$_['entry_multishop']        = 'Чи використовую я мультімагазін?';
$_['entry_off_description']  = 'Заради прискорення, пропускати перевірку на наявність тексту товару';
$_['help_off_description']   = 'При увімкненні цієї опції запит товарів буде працювати трохи швидше, але в карті можуть бути присутніми &quot;биті&quot; товари';
$_['entry_data_feed']        = 'Адреса карти сайту (підходить для всіх пошукових систем):';

$_['entry_data_feed_image'] = 'Додаткова карта з зображеннями товарів (Не підходить для Яндекса):';
$_['entry_limit_image']      = 'Ліміт товарів в одному файлі карти зображень:';
$_['help_limit_image']       = 'Робота з зображеннями сильно навантажується сервер';
$_['entry_require_image_caption'] = 'Чи включати для товарів опис для картинок';
$_['help_require_image_caption'] = 'Це трохи сповільнить Ваш Sitemap';
$_['entry_require_images']   = 'Чи включати для товарів додаткові зображення в карті для Google?';
$_['entry_off_check_image_file']  = 'Нахитрувати з зображеннями?';
$_['help_off_check_image_file']   = 'Значно знижує навантаження на сервер. Але в карті сайту можуть фігурувати неіснуючі посилання на зображення. Загалом, включивши цю опцію, стежте за звітами гугла :)';

// Button
$_['button_save_licence'] = 'Зберегти ліцензію';

// Error
$_['error_permission'] = 'У вас немає прав для управління цим розширенням!';
