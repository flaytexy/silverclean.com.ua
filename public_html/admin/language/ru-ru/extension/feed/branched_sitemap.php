<?php

/**
 * @category   OpenCart
 * @package    Branched Sitemap
 * @copyright  © Serge Tkach, 2018 - 2021, http://sergetkach.com/
 */

// Heading
$_['heading_title'] = 'Branched Sitemap';

// Text
$_['text_author']         = 'Автор';
$_['text_author_support'] = 'Поддержка';
$_['text_module_version'] = 'Версия модуля';
$_['text_system_version'] = 'Предназначено для системы версии';

$_['text_feed']      = 'Каналы продвижения';
$_['text_success']   = 'Настройки модуля обновлены!';
$_['text_edit']      = 'Редактировать Branched Sitemap';
$_['text_extension'] = 'Каналы продвижения';
$_['text_yes']       = 'Да';
$_['text_no']        = 'Нет';

// Entry
$_['entry_licence']          = 'Код лицензии:';
$_['entry_status']           = 'Статус:';
$_['entry_system']           = 'Используемая система:';
$_['entry_cachetime']        = 'Кешировать карту на:';
$_['cachetime_values_0']     = 'Не кэшировать';
$_['cachetime_values_1hour'] = '1 час';
$_['cachetime_values_6hours'] = '6 часов';
$_['cachetime_values_12hours'] = '12 часов';
$_['cachetime_values_24hours'] = 'Сутки';
$_['cachetime_values_1week'] = 'Неделя';
$_['entry_limit']            = 'Лимит товаров в одном файле:';
$_['help_limit']             = 'Чем слабее сервер, тем меньше должно быть значение';
$_['entry_multishop']        = 'Использую ли я мультимагазин?';
$_['entry_off_description']  = 'Ради ускорения, пропускать проверку на наличие текста товара';
$_['help_off_description']   = 'При включенной настройке запрос товаров будет работать немного быстрее, но в карте могут присутствовать &quot;битые&quot; товары';
$_['entry_data_feed']        = 'Адрес карты сайта (подходит для всех поисковиков):';

$_['entry_data_feed_image'] = 'Дополнительная карта с изображениями товаров (Не подходит для Яндекса):';
$_['entry_limit_image']      = 'Лимит товаров в одном файле карты изображений:';
$_['help_limit_image']       = 'Работа с изображениями сильно нагружает сервер';
$_['entry_require_image_caption'] = 'Включать ли для товаров описание для картинок';
$_['help_require_image_caption'] = 'Это немного замедлит Ваш Sitemap';
$_['entry_require_images']   = 'Включать ли для товаров дополнительные изображения в карте для Google?';
$_['entry_off_check_image_file']  = 'Похитрить с изображениями?';
$_['help_off_check_image_file']   = 'Значительно снижает нагрузку на сервер при открытии карты изображений. Но в карте могут фигурировать несуществующие ссылки на изображения. В общем, включив эту опцию, следите за отчетами гугла :)';

// Button
$_['button_save_licence'] = 'Сохранить лицензию';

// Error
$_['error_permission'] = 'У вас нет прав для управления этим модулем!';
