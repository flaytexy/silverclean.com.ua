<?php

// Heading
$_['heading_title']    = 'Демо-контент для шаблона Техникс';

// Text
$_['text_extension']   = 'Расширения';
$_['text_success']     = 'Демо контент установлен! Сохраните заново настройки шаблона. ';
$_['text_install']        = 'Установить контент';

// Button
$_['button_add']                    = 'Добавить контент';
$_['button_install_demo']                    = 'Установить демо-контент';

// Entry
$_['entry_status']     = 'Статус';

// Error
$_['error_permission'] = 'У вас недостаточно прав для внесения изменений!';