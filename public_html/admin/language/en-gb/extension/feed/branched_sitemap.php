<?php

/**
 * @category   OpenCart
 * @package    Branched Sitemap
 * @copyright  © Serge Tkach, 2018 - 2021, http://sergetkach.com/
 */

// Heading
$_['heading_title'] = 'Branched Sitemap';

// Text
$_['text_author']         = 'Author';
$_['text_author_support'] = 'Support';
$_['text_module_version'] = 'Extension version';
$_['text_system_version'] = 'For OpenCart version';

$_['text_feed']      = 'Extensions';
$_['text_success']   = 'Success: You have modified Branched Sitemap feed!';
$_['text_edit']      = 'Edit Branched Sitemap';
$_['text_extension'] = 'Extensions';
$_['text_yes']       = 'Yes';
$_['text_no']        = 'No';

// Entry
$_['entry_licence']          = 'Licence code:';
$_['entry_status']           = 'Status:';
$_['entry_system']           = 'Following System of OpenCart:';
$_['entry_cachetime']        = 'Cache sitemap for:';
$_['cachetime_values_0']     = 'Do not cache';
$_['cachetime_values_1hour'] = '1 hour';
$_['cachetime_values_6hours'] = '6 hours';
$_['cachetime_values_12hours'] = '12 hours';
$_['cachetime_values_24hours'] = 'A day';
$_['cachetime_values_1week'] = 'A week';
$_['entry_limit']            = 'Products limit per one file:';
$_['help_limit']             = 'The weaker the server, the less the value should be';
$_['entry_multishop']        = 'I use the multistore?';
$_['entry_off_description']  = 'For the sake of acceleration, skip checking for the text of the goods';
$_['help_off_description']   = 'When enabled, the request for goods will work a little faster, but there may be “broken” goods in the map';
$_['entry_data_feed']        = 'Data Feed Url for Google:';

$_['entry_data_feed_image'] = 'Data Feed Url for images:';
$_['entry_limit_image']      = 'Products limit per one file with image';
$_['help_limit_image']       = 'Image processing creates overload for server';
$_['entry_require_image_caption'] = 'Include a caption for the goods for the pictures';
$_['help_require_image_caption'] = 'This is a little slow down your sitemap';
$_['entry_require_images']   = 'Include additional images in the map for Google?';
$_['entry_off_check_image_file']  = 'Use savvy for image processing?';
$_['help_off_check_image_file']   = 'Significantly reduced server load when opening the sitemap for images. But non-existent image links may appear in the sitemap. In general, by enabling this option, follow Google reports :)';

// Button
$_['button_save_licence'] = 'Save Licence';

// Error
$_['error_permission'] = 'Warning: You do not have permission to modify Google Sitemap feed!';
